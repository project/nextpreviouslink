<?php

namespace Drupal\sos_common\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure end menu settings.
 *
 * @package Drupal\sos_common\Form
 */
class EndMenuTextForm extends ConfigFormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a SOS common object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheRender) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'end_menu_text_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['endmenu.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('endmenu.settings');
    $form['play_again'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Play Again'),
      '#default_value' => $config->get('play_again') ? $config->get('play_again') : $this->t('Play Again'),
      '#required' => TRUE,
    ];

    $form['next_case'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Next Case'),
      '#default_value' => $config->get('next_case') ? $config->get('next_case') : $this->t('Play Again'),
      '#required' => TRUE,
    ];

    $form['scoreboard'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scoreboard'),
      '#default_value' => $config->get('scoreboard') ? $config->get('scoreboard') : $this->t('Scoreboard'),
      '#required' => TRUE,
    ];

    $form['answer_sheets'] = [
      '#type' => 'text_format',
      '#format' => 'basic_html',
      '#title' => $this->t('Answer Sheets'),
      '#default_value' => $config->get('answer_sheets')['value'] ? $config->get('answer_sheets')['value'] : '',
    ];

    $form['exit_game'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Exit Game'),
      '#default_value' => $config->get('exit_game') ? $config->get('exit_game') : $this->t('Exit Game'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('endmenu.settings');
    $settings->set('play_again', $form_state->getValue('play_again'))->save();
    $settings->set('next_case', $form_state->getValue('next_case'))->save();
    $settings->set('scoreboard', $form_state->getValue('scoreboard'))->save();
    $settings->set('answer_sheets', $form_state->getValue('answer_sheets'))->save();
    $settings->set('exit_game', $form_state->getValue('exit_game'))->save();
    return parent::submitForm($form, $form_state);
  }

}
