<?php

namespace Drupal\sos_common\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure LaravelAPI settings.
 *
 * @package Drupal\sos_common\Form
 */
class LaravelApi extends ConfigFormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a SOS common object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheRender) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'laravel_api_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['laravelapi.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('laravelapi.settings');
    $form['conectados_api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $config->get('conectados_api_url') ? $config->get('conectados_api_url') : 'https://nginx-strikeorstroke-com-store-develop.bi-oneweb.com/api',
    ];
    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Token'),
      '#default_value' => $config->get('api_token') ? $config->get('api_token') : 'io659AA$hi!',
    ];
    $form['auth_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth username'),
      '#default_value' => $config->get('auth_username') ? $config->get('auth_username') : 'develop',
    ];
    $form['auth_pass'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth password'),
      '#default_value' => $config->get('auth_pass') ? $config->get('auth_pass') : 'bi!marketing',
    ];
    $form["conectados_api_key"] = [
      "#type" => "select",
      "#title" => $this->t("Environment"),
      "#description" => $this->t("API Environment to be used"),
      "#default_value" => $config->get('conectados_api_key'),
      "#options" => [
        "develop" => "Develop",
        "qa" => "QA",
        "production" => "Production",
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('laravelapi.settings');
    $settings->set('conectados_api_url', $form_state->getValue('conectados_api_url'))->save();
    $settings->set('api_token', $form_state->getValue('api_token'))->save();
    $settings->set('auth_username', $form_state->getValue('auth_username'))->save();
    $settings->set('auth_pass', $form_state->getValue('auth_pass'))->save();
    $settings->set('conectados_api_key', $form_state->getValue('conectados_api_key'))->save();
    return parent::submitForm($form, $form_state);
  }

}
