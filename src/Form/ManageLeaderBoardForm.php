<?php

namespace Drupal\sos_common\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure ManageLeaderBoardForm settings.
 *
 * @package Drupal\sos_common\Form
 */
class ManageLeaderBoardForm extends ConfigFormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a SOS common object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheRender) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_leader_board';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mlb.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mlb.settings');

    $form['final_score_top_1'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Final Score Top text for 0 to 50 percent'),
      '#default_value' => $config->get('final_score_top_1')['value'] ? $config->get('final_score_top_1')['value'] : '',
    ];

    $form['final_score_top_2'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Final Score Top text for 51 to 75 percent'),
      '#default_value' => $config->get('final_score_top_2')['value'] ? $config->get('final_score_top_2')['value'] : '',
    ];

    $form['final_score_top_3'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Final Score Top text for 76 to 100 percent'),
      '#default_value' => $config->get('final_score_top_3')['value'] ? $config->get('final_score_top_3')['value'] : '',
    ];

    $form['your_final_score_is'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your final score is Label'),
      '#default_value' => $config->get('your_final_score_is') ? $config->get('your_final_score_is') : $this->t('Your final score is'),
      '#required' => TRUE,
    ];

    $form['you_rank'] = [
      '#type' => 'textfield',
      '#title' => $this->t('You Rank Text'),
      '#default_value' => $config->get('you_rank') ? $config->get('you_rank') : $this->t('You Rank'),
      '#required' => TRUE,
    ];

    $form['no_rank'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No Rank Text'),
      '#default_value' => $config->get('no_rank') ? $config->get('no_rank') : $this->t('The Classification'),
      '#required' => TRUE,
    ];

    $form['scoreboard_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scoreboard Label'),
      '#default_value' => $config->get('scoreboard_text') ? $config->get('scoreboard_text') : $this->t('Scoreboard'),
      '#required' => TRUE,
    ];

    $form['no_scoreboard'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer Sheet warning text on pop up page'),
      '#default_value' => $config->get('no_scoreboard') ? $config->get('no_scoreboard') : '',
      '#required' => TRUE,
    ];

    $form['completed_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Completed Label'),
      '#default_value' => $config->get('completed_text') ? $config->get('completed_text') : $this->t('Completed'),
      '#required' => TRUE,
    ];

    $form['incomplete_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Incomplete Label'),
      '#default_value' => $config->get('incomplete_text') ? $config->get('incomplete_text') : $this->t('Incomplete'),
      '#required' => TRUE,
    ];

    $form['total'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Total Label'),
      '#default_value' => $config->get('total') ? $config->get('total') : $this->t('Total'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('mlb.settings');
    $settings->set('final_score_top_1', $form_state->getValue('final_score_top_1'))->save();
    $settings->set('final_score_top_2', $form_state->getValue('final_score_top_2'))->save();
    $settings->set('final_score_top_3', $form_state->getValue('final_score_top_3'))->save();
    $settings->set('your_final_score_is', $form_state->getValue('your_final_score_is'))->save();
    $settings->set('you_rank', $form_state->getValue('you_rank'))->save();
    $settings->set('no_rank', $form_state->getValue('no_rank'))->save();
    $settings->set('scoreboard_text', $form_state->getValue('scoreboard_text'))->save();
    $settings->set('no_scoreboard', $form_state->getValue('no_scoreboard'))->save();
    $settings->set('completed_text', $form_state->getValue('completed_text'))->save();
    $settings->set('incomplete_text', $form_state->getValue('incomplete_text'))->save();
    $settings->set('total', $form_state->getValue('total'))->save();
    return parent::submitForm($form, $form_state);
  }

}
