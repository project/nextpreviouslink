<?php

namespace Drupal\sos_common\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure end menu settings.
 *
 * @package Drupal\sos_common\Form
 */
class ManageCommanStaticForm extends ConfigFormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a SOS common object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheRender) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_comman_static_text_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mcs.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('mcs.settings');
    $form['back_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Back Button Label'),
      '#default_value' => $config->get('back_label') ? $config->get('back_label') : $this->t('Back'),
      '#required' => TRUE,
    ];

    $form['select_a_case'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select a Case Label'),
      '#default_value' => $config->get('select_a_case') ? $config->get('select_a_case') : $this->t('Select a Case'),
      '#required' => TRUE,
    ];

    $form['skip_intro'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Skip Intro Label'),
      '#default_value' => $config->get('skip_intro') ? $config->get('skip_intro') : $this->t('Skip Intro'),
      '#required' => TRUE,
    ];

    $form['answersheet_warning_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Answer Sheet warning button on pop up page'),
      '#default_value' => $config->get('answersheet_warning_button') ? $config->get('answersheet_warning_button') : $this->t('I want the answers!'),
      '#required' => TRUE,
    ];

    $form['answersheet_warning_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Answer Sheet warning text on pop up page'),
      '#default_value' => $config->get('answersheet_warning_text')['value'] ? $config->get('answersheet_warning_text')['value'] : '',
    ];

    $form['answer_top_text'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => $this->t('Top Text on Answer Page'),
      '#default_value' => $config->get('answer_top_text')['value'] ? $config->get('answer_top_text')['value'] : '',
    ];

    $form['all_answers_correct'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text for All Answers correct on Answer Page'),
      '#default_value' => $config->get('all_answers_correct') ? $config->get('all_answers_correct') : $this->t('All Answers are correct.'),
      '#required' => TRUE,
    ];

    $form['points_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for Points'),
      '#default_value' => $config->get('points_label') ? $config->get('points_label') : $this->t('Pts'),
      '#required' => TRUE,
    ];

    $form['question_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Question Label on Answer Page'),
      '#default_value' => $config->get('question_label') ? $config->get('question_label') : $this->t('QUESTION'),
      '#required' => TRUE,
    ];

    $form['correct_answer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Correct Answer Label on Answer Page'),
      '#default_value' => $config->get('correct_answer') ? $config->get('correct_answer') : $this->t('CORRECT ANSWER'),
      '#required' => TRUE,
    ];

    $form['continue_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Continue button on pop up'),
      '#default_value' => $config->get('continue_button') ? $config->get('continue_button') : $this->t('Continue'),
      '#required' => TRUE,
    ];

    $form['mute_unmute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mute/UnMute Speaker Button for Tracking'),
      '#default_value' => $config->get('mute_unmute') ? $config->get('mute_unmute') : $this->t('Mute/Unmute'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('mcs.settings');
    $settings->set('back_label', $form_state->getValue('back_label'))->save();
    $settings->set('select_a_case', $form_state->getValue('select_a_case'))->save();
    $settings->set('skip_intro', $form_state->getValue('skip_intro'))->save();
    $settings->set('answersheet_warning_button', $form_state->getValue('answersheet_warning_button'))->save();
    $settings->set('answersheet_warning_text', $form_state->getValue('answersheet_warning_text'))->save();
    $settings->set('answer_top_text', $form_state->getValue('answer_top_text'))->save();
    $settings->set('all_answers_correct', $form_state->getValue('all_answers_correct'))->save();
    $settings->set('points_label', $form_state->getValue('points_label'))->save();
    $settings->set('question_label', $form_state->getValue('question_label'))->save();
    $settings->set('correct_answer', $form_state->getValue('correct_answer'))->save();
    $settings->set('continue_button', $form_state->getValue('continue_button'))->save();
    $settings->set('mute_unmute', $form_state->getValue('mute_unmute'))->save();
    return parent::submitForm($form, $form_state);
  }

}
