<?php

namespace Drupal\sos_common\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Configure QuestionConfirmStatic settings.
 *
 * @package Drupal\sos_common\Form
 */
class QuestionConfirmStaticTextForm extends ConfigFormBase {

  /**
   * The cache render service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Constructs a SOS common object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheRender
   *   A cache backend interface instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cacheRender) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'question_confirm_text';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['qct.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('qct.settings');
    $form['first_question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Question'),
      '#default_value' => $config->get('first_question') ? $config->get('first_question') : $this->t('Would you be able to assist'),
      '#required' => TRUE,
    ];

    $form['first_ques_option1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Question Option 1'),
      '#default_value' => $config->get('first_ques_option1') ? $config->get('first_ques_option1') : $this->t('Yes'),
      '#required' => TRUE,
    ];

    $form['first_ques_option2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First Question Option 2'),
      '#default_value' => $config->get('first_ques_option2') ? $config->get('first_ques_option2') : $this->t('N'),
      '#required' => TRUE,
    ];

    $form['first_ques_option1_points'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Points on Game Proceed'),
      '#default_value' => $config->get('first_ques_option1_points') ? $config->get('first_ques_option1_points') : 100,
      '#required' => TRUE,
    ];

    $form['second_question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second Question'),
      '#default_value' => $config->get('second_question') ? $config->get('second_question') : $this->t('Are you sure'),
      '#required' => TRUE,
    ];

    $form['sec_ques_option1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second Question Option 1'),
      '#default_value' => $config->get('sec_ques_option1') ? $config->get('sec_ques_option1') : $this->t('Another time'),
      '#required' => TRUE,
    ];

    $form['sec_ques_option2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Second Question Option 2'),
      '#default_value' => $config->get('sec_ques_option2') ? $config->get('sec_ques_option2') : '',
      '#required' => TRUE,
    ];

    $form['second_ques_option1_points'] = [
      '#type' => 'number',
      '#title' => $this->t('Option 1 Points'),
      '#default_value' => $config->get('second_ques_option1_points') ? $config->get('second_ques_option1_points') : 50,
      '#required' => TRUE,
    ];

    $form['second_ques_option2_points'] = [
      '#type' => 'number',
      '#title' => $this->t('Option 2 Points'),
      '#default_value' => $config->get('second_ques_option2_points') ? $config->get('second_ques_option2_points') : 0,
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('qct.settings');
    $settings->set('first_question', $form_state->getValue('first_question'))->save();
    $settings->set('first_ques_option1', $form_state->getValue('first_ques_option1'))->save();
    $settings->set('first_ques_option2', $form_state->getValue('first_ques_option2'))->save();
    $settings->set('first_ques_option1_points', $form_state->getValue('first_ques_option1_points'))->save();
    $settings->set('second_question', $form_state->getValue('second_question'))->save();
    $settings->set('sec_ques_option1', $form_state->getValue('sec_ques_option1'))->save();
    $settings->set('sec_ques_option2', $form_state->getValue('sec_ques_option2'))->save();
    $settings->set('second_ques_option1_points', $form_state->getValue('second_ques_option1_points'))->save();
    $settings->set('second_ques_option2_points', $form_state->getValue('second_ques_option2_points'))->save();
    return parent::submitForm($form, $form_state);
  }

}
